﻿using System;
using System.Threading.Tasks;
using JWT.DTOs;
using JWT.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JWT.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticateController : Controller
    {

        private readonly UserService _userService;

        public AuthenticateController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet("hello")]
        public ActionResult Test()
        {
            try
            {
                return Ok(new { hello = "Hello" });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public ActionResult Login([FromBody] UserAuthDTO userAuthDto)
        {
            try
            {
                return Ok(_userService.Authenticate(userAuthDto));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}