﻿using AutoMapper;
using JWT.DTOs;
using JWT.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JWT.Services.Automapper
{
    public class AutoMapperProfile:Profile
    {
        public AutoMapperProfile()
        { 
            CreateMap<User, UserAuthDTO>().ReverseMap();
        }
    }
}
