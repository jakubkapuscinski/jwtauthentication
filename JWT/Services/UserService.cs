﻿using AutoMapper;
using JWT.DTOs;
using JWT.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JWT.Services
{
    public class UserService
    {
        
        private readonly IMapper _mapper;
        private readonly JWTTokenProviderService _jwtToken;

        public UserService(IMapper mapper, JWTTokenProviderService jwtToken)
        {
            _jwtToken = jwtToken;
            _mapper = mapper;
        }

        private readonly IEnumerable<User> _users = new List<User>
        {
             new User { Id = 1, Login = "Kasia", Password = "123"},
             new User { Id = 2, Login = "Tomek", Password = "456"},
             new User { Id = 3, Login = "Szymon", Password = "789"},
        };

        public UserAuthDTO Authenticate(UserAuthDTO userAuthDTO)
        {

            if (string.IsNullOrEmpty(userAuthDTO.Login) || string.IsNullOrEmpty(userAuthDTO.Password))
            {
                throw new Exception();
            }

            var user = _users.SingleOrDefault(x => x.Login == userAuthDTO.Login && x.Password == userAuthDTO.Password);
            
                string tokenString = _jwtToken.GenerateToken(user);
                var userWithToken = _mapper.Map<UserAuthDTO>(user);
                userWithToken.Token = tokenString;
                return  userWithToken;
        }

    }
}
