import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HelloPanelComponent } from './hello-panel/hello-panel.component';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path:  'login',
    component: LoginComponent,
  },
  {
    path: 'hellopanel',
    component: HelloPanelComponent,
    canActivate: [AuthGuard],
   },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
