import { Component, OnInit } from '@angular/core';
import { SecurityService } from '../services/security.service';
import * as jwt_decode from 'jwt-decode';
@Component({
  selector: 'app-hello-panel',
  templateUrl: './hello-panel.component.html',
  styleUrls: ['./hello-panel.component.css']
})
export class HelloPanelComponent implements OnInit {

  constructor(private securityService: SecurityService) { }
  tokenInfo = this.getDecodedToken();
  userLogin = this.tokenInfo.given_name;

  getDecodedToken(): any {
    try {
        return jwt_decode(localStorage.getItem('token'));
    } catch (Error) {
        return null;
    }
  }

ngOnInit() {
    this.securityService.getHelloPanel().subscribe(res => {
      console.log(res); });
  }

}
