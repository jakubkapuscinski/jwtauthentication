export class UserAuth {
  id: number;
  login: string;
  token: string;
}
