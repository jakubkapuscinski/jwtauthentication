import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserAuth } from '../data/user-auth';
import { UserLogin } from '../data/user-login';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};
const apiUrl = 'http://localhost:62376/api';

@Injectable({
  providedIn: 'root'
})

export class SecurityService {
  userAuth: UserAuth = {
    id: 0,
    login: '',
    token: ''
  };
  constructor(private http: HttpClient) { }

  login(userLogin: UserLogin): Observable<UserAuth> {
    return this.http.post<UserAuth>(apiUrl + '/Authenticate/login', userLogin, httpOptions);
  }

  getHelloPanel() {
    return this.http.get(apiUrl + '/Authenticate/hello');
  }

  isAuthenticated(): boolean {
    return localStorage.getItem('token') !== null;
  }

  resetUserAuth(): void {
    this.userAuth.login = null,
      this.userAuth.token = null,
      this.userAuth.id = null,
      localStorage.removeItem('token');
    localStorage.removeItem('id');
  }

  logout(): void {
    this.resetUserAuth();
    localStorage.clear();
  }
}

