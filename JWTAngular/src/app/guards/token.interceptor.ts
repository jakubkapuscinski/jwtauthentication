import { Injectable } from '@angular/core';
import {HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { SecurityService } from '../services/security.service';
import { Observable } from 'rxjs/internal/Observable';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(public securityService: SecurityService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = localStorage.getItem('token');

    if (token) {
            const cloned = request.clone({
                headers: request.headers.set('Authorization',
                    'Bearer ' + token)
            });
            return next.handle(cloned);
        } else {
            return next.handle(request);
        }
      }
    }
