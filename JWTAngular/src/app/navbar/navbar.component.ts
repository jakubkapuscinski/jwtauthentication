import { Component, OnInit } from '@angular/core';
import { SecurityService } from '../services/security.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  constructor(public securityService: SecurityService, private router: Router) {
  }

  logout(): void {
    this.securityService.logout();
    this.router.navigate(['/login']);
  }
  ngOnInit() {
  }

}
