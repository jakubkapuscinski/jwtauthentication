import { Component, OnInit } from '@angular/core';
import { UserLogin } from '../data/user-login';
import { UserAuth } from '../data/user-auth';
import { FormGroup, FormBuilder} from '@angular/forms';
import { SecurityService } from '../services/security.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userLogin: UserLogin;
  userAuth: UserAuth;
  loginForm: FormGroup;
  returnUrl: string;

  constructor(private formBuilder: FormBuilder,
              private securityService: SecurityService,
              private route: ActivatedRoute,
              private router: Router) { }

  login() {
    this.securityService.login(this.loginForm.value)
      .subscribe(
        result => {
          this.userAuth = result;
          localStorage.setItem('token', this.userAuth.token);
          if (this.securityService.isAuthenticated()) {
            this.router.navigateByUrl('/hellopanel');
          }
        },
      );
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
    this.loginForm = this.formBuilder.group({
      login: [''],
      password: ['']
    });
  }

}
